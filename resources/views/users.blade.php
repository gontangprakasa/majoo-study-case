@extends('layouts.admin')
@section('title', 'Users')
@section('contentCss')
<style>
div.dt-buttons{
	position:relative;
	float:right;
	margin-bottom: 10px;
}
</style>
@endsection
@section('contentJs')
<script>

    $('.modal').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });
    var token = '';

    table = $('#grdData').DataTable({
        processing: true, //Feature control the processing indicator.
        serverSide: true, //Feature control DataTables' server-side processing mode.
        ordering: true, //Initial no order.
        searching: true,
        info: false,
        responsive: true,
        ajax: {
            url: '{{ url("users/getAll") }}',
            beforeSend	: function(xhr){ 
                xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            },
            type: 'POST',
            dataType : 'json',
            data: function(d) {
                postFilter = new Object();				
                d.keyword = $('#keyword_search').val();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (errorThrown == 'Unauthorized') {
                    Swal.fire("Perhatian", "Session Expired!" , "info");
                    location.reload();
                }
            }
        },
        order: [[ 1, "asc" ]],
        fnServerParams: function(data) {
            console.log("data"+data);
        data['order'].forEach(function(items, index) {
            data['order'][index]['column'] = data['columns'][items.column]['data'];
            });
        },
        columns: [
            { data: "id", name: "id", orderable: false,
                render: function(data, type, row, meta){
                    return (meta.row+1);
                }
            },
			{ data: "fullname", name: "fullname"},
            { data: "email", name: "email"},
            { data: "role_name", name: "role_name"},
            { data: "id", name: "id", orderable: false, 
                render: function(data, type, row, meta){
                    var elShow = '<a class="btn btn-sm btn-default" href="javascript: editData(\''+row.id+'\');"><i class="fa fa-eye"></i></a>';
                    // if (row.role_id != 1) {
						return '<div class="btn-group">\
							'+elShow+'\
						</div>';
					// } else {
					// 	return '<div class="btn-group"><div>';
					// }
                } 			
            },
        ],
        columnDefs: [
            {
                targets: [1], 
                className: 'text-center',
            }
        ],
        lengthChange: true,
        pagingType: 'numbers',
        pageLength: 50,
        aLengthMenu: [
            [10, 25, 50, 100, 10000000],
            [10, 25, 50, 100, "All"]
        ],
        dom: 'lrt<"row"<"col-md-6"><"col-md-6"p>><"clear">i',
        initComplete: function(settings, json) {
        },			
    });

    $('#frmData').submit(function(){
		var formData = new FormData($('#frmData')[0]);
		$.ajax({
			type: 'POST',
			headers: {'X-CSRF-TOKEN': csrfToken},
            data: formData,
			url : "{{ url('users/create') }}", 
            contentType: false,
            processData: false,   
            cache: false,
			selectorBlock: '#dlgData .modal-content',
			selectorAlert: '#alertData',
			success : function(ret){
				if (ret.result == true) {
					$('#dlgData').modal('hide');
                    Swal.fire({
						icon: 'success',
						title: 'Data berhasil disimpan',
						showConfirmButton: false,
						timer: 3000
					});
					table.draw();
				} else {
					Swal.fire({
						title: '<strong>Gagal</strong>',
						icon: 'error',
						html: ret.msg ,
						showConfirmButton: false,
						timer: 3000
					});
				}
			},
		});
	});

    function editData(id){
        postData = 		new Object();
		postData.id = 	id;
		ajax({
			url : "{{ url('users/getOne') }}", 
			postData : postData,
			success : function(ret){
				$('#dlgData').modal('show');
				var data = ret.data;
				$('.title-result').html("Update")
                $('#mode').val("edit");
                $('#id').val(data.id);
                $('#fullname').val(data.fullname);
                $('#role_id').val(data.role_id).trigger('change');
            }
		});
    }

	$('#dlgData').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });

    $('#filter_search').click(function(e){
        table.draw();
    });

</script>
@endsection

@section('content')
<div class="row">
	<div class="row col-md-12">
		<div class="">
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-9">
					<div class="form-group has-feedback">
						<input type="text" name="keyword_search" class="form-control" placeholder="Keyword Fullname" id="keyword_search" autocomplete="off">
						<span class="fa fa-search form-control-feedback"></span>
					</div>
				</div>
				<div class="col-md-3">
					<button type="submit" class="btn btn-primary pull-right" id="filter_search" style="width: 100%">
						<span class="fa fa-filter"> Filter</span>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12 table-responsive">
		<table id="grdData" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th style="width: 20px">No</th>
					<th style="width: ;">Full Name</th>
                    <th style="width: ;">Email</th>
                    <th style="width: ;">Role</th>
					<th style="width: 80px;">Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>

<div id="dlgData" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><b class="title-result"></b> Category </h4>
			</div>
			<form class="form-horizontal" id="frmData" onSubmit="return false" method="post" enctype="multipart/form-data" action="#">
			@csrf
				<div class="modal-body">
					<div id="alertData" style="display: none;"></div>
					<input type="hidden" name="mode" value="add" id="mode">
					<input type="hidden" name="id" id="id">

                    <div class="form-group">
						<label class="col-sm-3 control-label" for="fullname">Full Name </label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="fullname" name="fullname" placeholder="Full Name" required value="">
						</div>
					</div>

                    <div class="form-group">
						<label class="col-sm-3 control-label" for="role_id">Roles</label>
						<div class="col-sm-9">
							<select name="role_id" id="role_id" class="select2" required>
									<option value="" hidden>Select Category </option>
									@foreach(App\Models\Roles::pluck('role_name','id') as $id => $label)
										<option value="{{ $id }}">{{ $label }}</option>
									@endforeach
							</select>
						</div>
					</div>
				</div>
					
				<div class="modal-footer">
					<button type="submit" class="btn btn-default" id="save-data">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>


@endsection
