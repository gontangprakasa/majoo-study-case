<?php
	$currentAction = Route::currentRouteAction();
	list($controller, $method) = explode('@', $currentAction);
	$controller = str_replace("App\\Http\\Controllers\\", "", $controller);
?>
<section class="sidebar">
	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<li class="{{ ($controller=='DashboardController'?'active':'') }}">
			<a href="{{ url('dashboard') }}">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
			</a>
		</li>

        @if(in_array(Auth::User()->role_id, [1]))
            <li class="{{ ($controller=='CategoriesController'?'active':'') }}">
                <a href="{{ url('categories') }}">
                    <i class="fa fa-list-alt"></i> <span>Categories</span>
			    </a>
		    </li>
            <li class="{{ ($controller=='ProductsController'?'active':'') }}">
                <a href="{{ url('products') }}">
                    <i class="fa fa-product-hunt"></i> <span>Products</span>
			    </a>
		    </li>
        <hr>
			<li class="{{ ($controller=='ProductsUserController'?'active':'') }}">
                <a href="{{ url('products-user') }}">
                    <i class="fa fa-list-alt"></i> <span>Products User List</span>
			    </a>
		    </li>
		<hr>

            <li class="{{ ($controller=='UsersController'?'active':'') }}">
                <a href="{{ url('users') }}">
                    <i class="fa fa-users"></i> <span>Users</span>
			    </a>
		    </li>
            <li class="{{ ($controller=='RolesController'?'active':'') }}">
                <a href="{{ url('roles') }}">
                    <i class="fa fa-tasks"></i> <span>Roles</span>
			    </a>
		    </li>
        @endif
        <hr>
		
	</ul>
</section>