@extends('layouts.page')
@section('title', 'Home')
@section('contentCss')
<style>
div.dt-buttons{
	position:relative;
	float:right;
	margin-bottom: 10px;
}
</style>
@endsection
@section('contentJs')
<script>
    $(document).ready(function() {
        $.ajax({ 
            url: '{{ url("page/loadListProductBy") }}',
            type: "POST",
            beforeSend	: function(xhr){ 
                xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            },
            dataType: "json",  
            data: {
                id: {{ Auth::user()->id }}
            },             
            success:function(data){
                $('#count_data').html(data.data.length);
                $.each(data.data, function(key, value) {
                    $(".load_products").append("<div class='col mb-5'>\
                        \<div class='card h-100'>\
                            <img class='card-img-top' width='250px' height='250px' src="+value.file+" alt='...' />\
                            <div class='card-body p-4'>\
                            <div class='text-center'>\
                                    <h5 class='fw-bolder'>"+value.product_name+"</h5>\
                                    <h6 class='fw-bolder'>Price : "+formatRupiah(parseInt(value.price).toString(), 'Rp. ')+"</h6>\
                                    <h6 class='fw-bolder'>Total Price : "+formatRupiah(parseInt((value.price) * value.quality).toString() , 'Rp. ')+"</h6>\
                                </div>\
                            </div>\
                            <div class='text-left p-4'>\
                                "+value.description+"\
                            </div>\
                            <div class='card-footer p-4 pt-0 border-top-0 bg-transparent'>\
                                <div class='text-center'><a class='btn btn-outline-dark mt-auto' >Qty : "+value.quality+"</a></div>\
                            </div>\
                        </div>\
                    </div>");
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (errorThrown == 'Unauthorized') {
                    Swal.fire("Perhatian", "Session Expired!" , "info");
                    location.reload();
                }
            }
        });
    });

    function buyNow(id){
        var check_auth = "{{ Auth::user() }}"
        if(check_auth == false ) {
            Swal.fire({
                title: '<strong>Please Login first!!!</strong>',
                icon: 'info',
                }).then(function() {
                    window.location.href = "/login";
                })
        } else {
            
            postData = 		new Object();
            postData.id = 	id;
            $.ajax({
                url : "{{ url('products/getOne') }}", 
                type: "POST",
                data : postData,
                beforeSend	: function(xhr){ 
                    xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                success : function(ret){
                    $('#showBuy').modal('show');
                    var data = ret.data;
                    $('#product_name').html(data.product_name)
                    $('#price').val(formatRupiah(parseInt(data.price).toString(), 'Rp. '))
                    $('#price_pure').val(parseInt(data.price).toString())
                    $('#product_id').val(data.id)
                }
            });
            
        }
    }

    $('#close').on('click' , function() { 
        $('#showBuy').modal('hide')
    });

    function countPrice() {
        var quality = $('#quality').val();
        var price = $('#price_pure').val();
        $('#count_price').val(formatRupiah(parseInt(quality * price).toString(), 'Rp. '))
    }

    $('#frmDataBuyNow').submit(function(){
        var formData = new FormData($('#frmDataBuyNow')[0]);
        $.ajax({
            type: 'POST',
            beforeSend	: function(xhr){ 
                xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            },
            data: formData,
            url : "{{ url('products-user/create') }}", 
            contentType: false,
            processData: false,   
            cache: false,
            selectorBlock: '#dlgData .modal-content',
            selectorAlert: '#alertData',
            success : function(ret){
                if (ret.result == true) {
                    $('#dlgData').modal('hide');
                    Swal.fire({
                        icon: 'success',
                        title: 'Data berhasil disimpan',
                        showConfirmButton: false,
                        timer: 3000
                    }).then(function(){
                        $('#showBuy').modal('hide')
                    });
                } else {
                    Swal.fire({
                        title: '<strong>Gagal</strong>',
                        icon: 'error',
                        html: ret.msg ,
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            },
        });
    });

    $('#showBuy').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });
    
</script>
@endsection

@section('content')
<!-- Header-->
<header class="bg-dark py-5">
    <div class="container px-4 px-lg-5 my-5">
        <div class="text-center text-white">
            <h1 class="display-4 fw-bolder">Shop in {{ env('APP_NAME') }}</h1>
            <p class="lead fw-normal text-white-50 mb-0">Marketplace Terlengkap no 1 di Indonesia</p>
        </div>
    </div>
</header>
<!-- Section-->
<section class="py-5">
    <h4 class="text-center">List Product Anda</h4>
    <div class="container px-4 px-lg-5 mt-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center load_products">
        </div>
    </div>
    <center>
        <div class="pagination">
            <a href="#">&laquo;</a>
            @for($i = 1 ; $i <= 5 ;  $i++)
                <a href="#">{{ $i }}</a>
            @endfor
            <a href="#">&raquo;</a>
        </div>
    </center>
</section>
<!-- Footer-->
<footer class="py-5 bg-dark">
    <div class="container"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2022</p></div>
</footer>
@endsection

