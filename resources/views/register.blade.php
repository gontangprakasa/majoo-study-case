@extends('layouts.login')
@section('title', 'Login')
@section('contentCss')
<style>

</style>
@endsection
@section('contentJs')
<script>
	$(document).ready(function(){
		$('#frmRegister').submit(function(){
			postData = new Object();
			$.each($('#frmRegister :input').serializeObject(), function(x, y){ postData[x]=y; });

			ajax({
				url : "{{ url('register') }}", 
				postData : postData,
				success : function(ret){
					setTimeout(function(){
						blockUI('body', true, 'Please wait', '300px');
						window.location.replace(ret.redirect);
					}, 100);
				},
			});			
		});			
	});
</script>
@endsection
@section('content')
<p class="login-box-msg">Register</p>
<form id="frmRegister" onSubmit="return false" method="post">
    <div class="form-group has-feedback">
		<input id="full_name" type="text" class="form-control" placeholder="Full Name">
		<span class="glyphicon glyphicon-user form-control-feedback"></span>
	</div>
	<div class="form-group has-feedback">
		<input id="email" type="email" class="form-control" placeholder="Email">
	</div>
    <div class="form-group has-feedback">
		<input id="password" type="password" class="form-control" placeholder="Password">
	</div>
    <div class="form-group has-feedback">
		<input id="password_confirmation" type="password" class="form-control" placeholder="Password Confirmation">
	</div>
	<div class="row">
		<div class="col-xs-12 text-right">
			<button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
		</div>
		<div class="col-xs-12 text-right">
			<a href="/login" >Sudah punya akun?</a>
		</div>
	</div>
</form>
@endsection