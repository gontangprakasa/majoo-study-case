@extends('layouts.admin')
@section('title', 'Change Password')
@section('contentCss')
<style>

</style>
@endsection
@section('contentJs')
<script>
	 $('#formData').submit(function(){
		var formData = new FormData($('#formData')[0]);
		$.ajax({
			type: 'POST',
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: formData,
			url : "{{ url('change-password') }}", 
            contentType: false,
            processData: false,   
            cache: false,
			success : function(ret){
				if (ret.result == true) {
					Swal.fire("Berhasil", ret.msg , "success");
				} else {
					Swal.fire({
						title: '<strong>Gagal</strong>',
						icon: 'error',
						html: ret.msg ,
						showCloseButton: true,
					})
				}
			},
		});
	});
</script>
@endsection
@section('content')
&nbsp;

<div class="row">
        
    <form id="formData" onSubmit="return false" method="post" enctype="multipart/form-data" action="#">
        @csrf
        <div class="col-md-10">
            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="" value="{{ $user->fullname }}" disabled>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="email" value="{{ $user->email }}" readonly>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback {{ session('errors') == true ? 'has-error' : '' }}">
                <input type="password" class="form-control" name="old_password" value="{{ old('old_password') }}" placeholder="Old Password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback {{ session('errors') == true ? 'has-error' : '' }}">
                <input type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="New Password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback {{ session('errors') == true ? 'has-error' : '' }}">
                <input type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Confirm Password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary pull-right">
                        <i class="fa fa-btn fa-lock"></i> Reset Password
                    </button>
                </div>
            </div>
        </div>
    </form>		
</div>
@endsection