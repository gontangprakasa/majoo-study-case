@extends('layouts.admin')
@section('title', 'Products')
@section('contentCss')
<style>
div.dt-buttons{
	position:relative;
	float:right;
	margin-bottom: 10px;
}

/* .progress 
{
  display:none; 
  position:relative; 
  width:400px; 
  border: 1px solid #ddd; 
  padding: 1px; 
  border-radius: 3px; 
}
.bar 
{ 
  background-color: #B4F5B4; 
  width:0%; 
  height:20px; 
  border-radius: 3px; 
}
.percent 
{ 
  position:absolute; 
  display:inline-block; 
  top:3px; 
  left:48%; 
} */
</style>
@endsection
@section('contentJs')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
	CKEDITOR.replace('description');

    $('.modal').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    });
    var token = '';

    table = $('#grdData').DataTable({
        processing: true, //Feature control the processing indicator.
        serverSide: true, //Feature control DataTables' server-side processing mode.
        ordering: true, //Initial no order.
        searching: true,
        info: false,
        responsive: true,
        ajax: {
            url: '{{ url("products/getAll") }}',
            beforeSend	: function(xhr){ 
                xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            },
            type: 'POST',
            dataType : 'json',
            data: function(d) {
                postFilter = new Object();				
                d.keyword = $('#keyword_search').val();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (errorThrown == 'Unauthorized') {
                    Swal.fire("Perhatian", "Session Expired!" , "info");
                    location.reload();
                }
            }
        },
        order: [[ 1, "asc" ]],
        fnServerParams: function(data) {
            console.log("data"+data);
        data['order'].forEach(function(items, index) {
            data['order'][index]['column'] = data['columns'][items.column]['data'];
            });
        },
        columns: [
            { data: "id", name: "id", orderable: false,
                render: function(data, type, row, meta){
                    return (meta.row+1);
                }
            },
			{ data: "category_name", name: "category_name"},
			{ data: "product_name", name: "product_name"},
			{ data: "price", name: "price", orderable: false, 
                render: function(data, type, row, meta){
					return formatRupiah(parseInt(row.price).toString(), 'Rp. ');
                } 			
            },
            { data: "id", name: "id", orderable: false, 
                render: function(data, type, row, meta){
                    var elShow = '<a class="btn btn-sm btn-default" href="javascript: editData(\''+row.id+'\');"><i class="fa fa-eye"></i></a>';
                    var elDelete = '<a class="btn btn-sm btn-default" href="javascript: deleteData(\''+row.id+'\', \''+row.product_name+'\');"><i class="fa fa-trash"></i></a>';
                    return '<div class="btn-group">\
							'+elShow+'\
							'+elDelete+'\
						</div>';
                } 			
            },
        ],
        columnDefs: [
            {
                targets: [1], 
                className: 'text-center',
            }
        ],
        lengthChange: true,
        pagingType: 'numbers',
        pageLength: 50,
        aLengthMenu: [
            [10, 25, 50, 100, 10000000],
            [10, 25, 50, 100, "All"]
        ],
        dom: 'lrt<"row"<"col-md-6"><"col-md-6"p>><"clear">i',
        initComplete: function(settings, json) {
        },			
    });

    $('#frmData').submit(function(){
		for ( instance in CKEDITOR.instances ) {
			CKEDITOR.instances[instance].updateElement();
		}
		
		var formData = new FormData($('#frmData')[0]);
		$.ajax({
			type: 'POST',
			headers: {'X-CSRF-TOKEN': csrfToken},
            data: formData,
			url : "{{ url('products/create') }}", 
            contentType: false,
            processData: false,   
            cache: false,
			selectorBlock: '#dlgData .modal-content',
			selectorAlert: '#alertData',
			xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = ((evt.loaded / evt.total) * 100);
                        $(".progress-bar").width(percentComplete + '%');
                        $(".progress-bar").html(percentComplete+'%');
                    }
                }, false);
                return xhr;
            },
			beforeSend: function(){
                $(".progress-bar").width('0%');
                $('#uploadStatus').html('<img src="loading.gif"/>');
            },
			error: function (response, status, e) {
				alert('Oops something went.');
			},
			success : function(ret){
				setTimeout(function(){
					if (ret.result == true) {
						$('#dlgData').modal('hide');
						Swal.fire({
							icon: 'success',
							title: 'Data berhasil disimpan',
							showConfirmButton: false,
							timer: 3000
						});
						table.draw();
					} else {
						Swal.fire({
							title: '<strong>Gagal</strong>',
							icon: 'error',
							html: ret.msg ,
							showConfirmButton: false,
							timer: 3000
						});
					}
				},3000)
			},
		});
	});

    function editData(id){
        postData = 		new Object();
		postData.id = 	id;
		ajax({
			url : "{{ url('products/getOne') }}", 
			postData : postData,
			success : function(ret){
				$('#dlgData').modal('show');
				var data = ret.data;
				$('.title-result').html("Update")
                $('#mode').val("edit");
                $('#id').val(data.id);
                $('#product_name').val(data.product_name);
				CKEDITOR.instances.description.setData(data.description);
				var rupiah = parseInt(data.price)
				$('#price').val(formatRupiah(rupiah.toString(), 'Rp. '));
				$('#category_id').val(data.category_id).trigger('change');
				$('#file').removeAttr('required','required');
				$('#showImage').html("<a href='"+data.file+"' data-target='#myModal' data-toggle='modal'>Show Image?</a>");
            }
		});
    }

	function deleteData(id, name){
		conf = confirm("Are you sure want to delete "+name+" ?");
		if( conf ){
			postData = new Object();
			postData.id = id;

			ajax({
				url : "{{ url('products/delete') }}", 
				postData : postData,
				success : function(ret){
					alertBox('show', {msg: name+' berhasil dihapus', mode: 'success'});
					table.draw();
				},
			});		
		}
	}

	
	$('#btnAdd').click(function(e){
		alertBox('hide', {selectorAlert: '#alertData'});
		$('.modal-footer').removeAttr('style');
		$('.title-result').html("Create")
		$('#token').val(token);
		$('#mode').val("add");
        $('#id').val(null);
		CKEDITOR.instances.description.setData('');
		$('#dlgData').modal('show');
		$('#showImage').html();
	});

	$('#dlgData').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
		$('#showImage').html("");
		$('#category_id').val('').trigger('change');
    });

	$('#myModal').on('show.bs.modal', function (e) {
        var img = $(e.relatedTarget).attr('href'); // get image with <a> tag
        $('#showimg').attr('src' , img); //load image in modal
    });

    $('#filter_search').click(function(e){
        table.draw();
    });

	$( document ).ready(function() {
		let category = "{{ $count_category }}" ;
		if (category == 0) {
			Swal.fire({
                    title: '<strong>Please fill category first!!!</strong>',
                    icon: 'info',
                }).then(function() {
                    window.location.href = "/categories";
                })
		}
	});

</script>


@endsection

@section('content')
<div class="row">
	<div class="row col-md-12">
		<div class="">
			<div class="col-md-12">  
				<button id="btnAdd" class="btn btn-default">Add Product</button>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-9">
					<div class="form-group has-feedback">
						<input type="text" name="keyword_search" class="form-control" placeholder="Keyword Product" id="keyword_search" autocomplete="off">
						<span class="fa fa-search form-control-feedback"></span>
					</div>
				</div>
				<div class="col-md-3">
					<button type="submit" class="btn btn-primary pull-right" id="filter_search" style="width: 100%">
						<span class="fa fa-filter"> Filter</span>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12 table-responsive">
		<table id="grdData" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th style="width: 20px">No</th>
					<th style="width: ;">Category Name</th>
					<th style="width: ;">Product Name</th>
					<th style="width: ;">Price</th>
					<th style="width: 80px;">Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>

<div id="dlgData" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><b class="title-result"></b> Product </h4>
			</div>
			<form class="form-horizontal" id="frmData" onSubmit="return false" method="post" enctype="multipart/form-data" action="#">
			@csrf
				<div class="modal-body">
					<div id="alertData" style="display: none;"></div>
					<input type="hidden" name="mode" value="add" id="mode">
					<input type="hidden" name="id" id="id">

					<div class="form-group">
						<label class="col-sm-3 control-label" for="category_id">Category</label>
						<div class="col-sm-9">
							<select name="category_id" id="category_id" class="select2" required>
									<option value="" hidden>Select Category </option>
									@foreach(App\Models\Categories::pluck('category_name','id') as $id => $label)
										<option value="{{ $id }}">{{ $label }}</option>
									@endforeach
							</select>
						</div>
					</div>

                    <div class="form-group">
						<label class="col-sm-3 control-label" for="product_name">Product Name </label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product Name" required value="">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="price">Price </label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="price" name="price" placeholder="Price" required value="">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="description">Description Name </label>
						<div class="col-sm-9">
							<textarea name="description" id="description" class="textarea myeditor" ></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="file">File </label>
						<div class="col-sm-9">
							<input type="file" class="form-control" id="file" name="file" placeholder="File" required value="">
							<div id="showImage"></div>
						</div>
					</div>
					<div class="progress">
						<div class="progress-bar"></div>
					</div>
					
				</div>
					
				<div class="modal-footer">
					<button type="submit" class="btn btn-default" id="save-data">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Show Image</h4>

			</div>
			<div class="modal-body">
				<img class="img-responsive" src="" alt="" id="showimg">
			</div>
		</div>
	</div>
</div>


@endsection
