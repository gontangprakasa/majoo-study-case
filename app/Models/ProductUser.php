<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductUser extends Model
{
    protected $table = "product_users";
	protected $primaryKey = "id";
	public $incrementing = true;	
}
