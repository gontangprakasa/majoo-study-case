<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use App\Repositories\ProductsRepository;

class PageController extends Controller
{

    
    private $products;
    public function __construct(ProductsRepository $productsRepository)
    {
        $this->products = $productsRepository;
    }

    public function index() 
    {
        $user_id = @Auth::user()->id;
        if (@$user_id) {
            $request['id'] = $user_id;
            $data = $this->products->loadListProductBy($request);
            $count_data = count($data->data);
        } else {
            $count_data = 0;
        }
        
        return view('page')->with([
            'count_data' =>  $count_data
        ]);
    }

    public function loadProductBy()
    {
        
        return view('page-list-product');
    }

    public function loadProduct(Request $request)
    {
        try {
           $data = $this->products->loadProduct($request->all());
           return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function loadListProductBy(Request $request)
    {
        try {
           $data = $this->products->loadListProductBy($request->all());
           return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }
}
