<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\Users;
use Illuminate\Support\Facades\Mail;

use App\Repositories\UsersRepository;

class LoginController extends Controller
{
	protected $users;

	public function __construct( UsersRepository $usersRepository)
	{
		$this->users = $usersRepository;
	}

	public function index() {
		return view('login');
	}

    public function login(Request $request) {
		try {
            $data  = $this->users->login($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }
	
	public function logout() {
		$url = '/';
		Auth::logout();
		session()->flush();
		return redirect($url);
	}
}