<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use App\Repositories\UsersRepository;

class UsersController extends Controller
{

    private $users;
    public function __construct(UsersRepository $usersRepository)
    {
        $this->users = $usersRepository;
    }

    public function index() {
        return view('users');
    }

    public function getAll(Request $request) {
        try {
            $data  = $this->users->getAll($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function getOne(Request $request) {
        try {
            $data  = $this->users->getOne($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function create(Request $request)
    {
        try {
            $element_checks = [
              'fullname'    => 'required',
              'role_id'     => 'required|int|exists:roles,id',
            ];
            $element_attributes = [
                'fullname'    => 'Full Name',
                'role_id'     => 'Role',
            ];

            $validator = Validator::make($request->all(), $element_checks)->setAttributeNames($element_attributes);
            if ($validator->fails()) {
                $res["result"]  = false;
                $res["msg"]     = $validator->messages()->all();
                return response()->json($res);
            }

            $simpan_data = $this->users->create($request->all());
            return response()->json($simpan_data,200); 

        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

}
