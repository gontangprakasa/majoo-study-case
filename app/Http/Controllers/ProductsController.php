<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use App\Repositories\ProductsRepository;
use App\Repositories\CategoriesRepository;

class ProductsController extends Controller
{

    private $products;
    private $categories;
    public function __construct(ProductsRepository $productsRepository, CategoriesRepository $categoriesRepository)
    {
        $this->products = $productsRepository;
        $this->categories = $categoriesRepository;
    }

    public function index() {
        $data = $this->categories->getCount();
        
        return view('products')->with([
            'count_category' => $data->data
        ]);
    }

    public function getAll(Request $request) {
        try {
            $data  = $this->products->getAll($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function getOne(Request $request) {
        try {
            $data  = $this->products->getOne($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function create(Request $request)
    {
        try {
            $element_checks = [
                'product_name'        => $request->input('mode') == 'add' ? 'required|string|min:1|unique:products,product_name':'required|string|min:1|unique:products,product_name,'.$request->input('id'),
                'category_id'         => 'required|int|exists:categories,id',
                'description'         => 'required',
                'file'                => $request->input('mode') == 'add' ? 'required|mimes:jpeg,png,jpg' : 'sometimes|mimes:jpeg,png,jpg',
                'price'               => 'required',
            ];
            $element_attributes = [
                'product_name'        => 'Product Name',
                'category_id'         => 'Category',
                'description'         => 'Description',
                'file'                => 'File',
                'price'               => 'Price',
            ];

            $validator = Validator::make($request->all(), $element_checks)->setAttributeNames($element_attributes);
            if ($validator->fails()) {
                $res["result"]  = false;
                $res["msg"]     = $validator->messages()->all();
                return response()->json($res);
            }

            $simpan_data = $this->products->create($request->all(), $request->hasFile("file"), $request->file('file'));
            return response()->json($simpan_data,200); 

        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function delete(Request $request) {
        try {
            $data  = $this->products->delete($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

}
