<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use App\Repositories\ProductsRepository;
use App\Repositories\UsersRepository;

class ProductsUserController extends Controller
{

    private $products;
    private $users;
    public function __construct(ProductsRepository $productsRepository, UsersRepository $usersRepository)
    {
        $this->products = $productsRepository;
        $this->users = $usersRepository;
    }

    public function index() {
        return view('products/product-user-list');
    }

    public function getIndexOne($id) {
        return view('products/product-user-list-result')->with([
            'id' => $id
        ]);
    }

    public function getAll(Request $request) {
        try {
            $data  = $this->users->getBy('Customer',$request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function getOne(Request $request) {
        try {
            $data  = $this->products->getOne($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function createOrder(Request $request) {
        try {
            $element_checks = [
                'product_id'               => 'required',
                'quality'                  => 'required',
            ];
            $element_attributes = [
                'product_id'               => 'Product',
                'quality'                  => 'Quality',
            ];

            $validator = Validator::make($request->all(), $element_checks)->setAttributeNames($element_attributes);
            if ($validator->fails()) {
                $res["result"]  = false;
                $res["msg"]     = $validator->messages()->all();
                return response()->json($res);
            }

            $data  = $this->products->createOrder($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

}
