<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Models\Users;
use Illuminate\Support\Facades\Mail;

use App\Repositories\UsersRepository;

class RegisterController extends Controller
{
	protected $users;

	public function __construct( UsersRepository $usersRepository)
	{
		$this->users = $usersRepository;
	}

	public function index() {
		return view('register');
	}

    public function register(Request $request) {
		try {
            $element_checks = [
                'full_name'               => 'required',
                'email'                   => 'required|email|unique:users,email',
                'password'                => 'required|min:6|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation'   => 'required|min:6',
            ];
            $element_attributes = [
                'full_name'             => 'Full Name',
                'email'                 => 'Email',
                'password'              => 'New Password',
                'password_confirmation' => 'New Password Confirmation',
            ];

            $validator = Validator::make($request->all(), $element_checks)->setAttributeNames($element_attributes);
            if ($validator->fails()) {
                $res["result"]  = false;
                $res["msg"]     = $validator->messages()->all();
                return response()->json($res);
            }

            $data  = $this->users->register($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }
}