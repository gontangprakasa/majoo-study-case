<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use App\Repositories\CategoriesRepository;

class CategoriesController extends Controller
{

    private $categories;
    public function __construct(CategoriesRepository $categoriesRepository)
    {
        $this->categories = $categoriesRepository;
    }

    public function index() {
        return view('categories');
    }

    public function getAll(Request $request) {
        try {
            $data  = $this->categories->getAll($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function getOne(Request $request) {
        try {
            $data  = $this->categories->getOne($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function create(Request $request)
    {
        try {
            $element_checks = [
                'category_name'        => $request->input('mode') == 'add' ? 'required|string|min:1|unique:categories,category_name':'required|string|min:1|unique:categories,category_name,'.$request->input('id'),
            ];
            $element_attributes = [
                'category_name'        => 'Category Name',
            ];

            $validator = Validator::make($request->all(), $element_checks)->setAttributeNames($element_attributes);
            if ($validator->fails()) {
                $res["result"]  = false;
                $res["msg"]     = $validator->messages()->all();
                return response()->json($res);
            }

            $simpan_data = $this->categories->create($request->all());
            return response()->json($simpan_data,200); 

        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function delete(Request $request) {
        try {
            $data  = $this->categories->delete($request->all());
            
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }


}
