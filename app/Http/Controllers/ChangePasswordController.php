<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use App\Repositories\UsersRepository;

class ChangePasswordController extends Controller
{

    private $users;
    public function __construct(UsersRepository $usersRepository)
    {
        $this->users = $usersRepository;
    }

    public function index() {
        $request['id'] = Auth::user()->id;
        $data  = $this->users->getOne($request);
        return view('change-password')->with([
            'user' => $data->data
        ]);
    }

    public function getOne($id) {
        try {
            $data  = $this->users->getOne($id);
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function create(Request $request)
    {
        try {
            $element_checks = [
                'old_password'            => 'required',
                'password'                => 'required|min:6|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation'   => 'required|min:6',
            ];
            $element_attributes = [
                'old_password'          => 'Old Password',
                'password'              => 'New Password',
                'password_confirmation' => 'New Password Confirmation',
            ];

            $validator = Validator::make($request->all(), $element_checks)->setAttributeNames($element_attributes);
            if ($validator->fails()) {
                $res["result"]  = false;
                $res["msg"]     = $validator->messages()->all();
                return response()->json($res);
            }

            $simpan_data = $this->users->changePassword($request->all());
            return response()->json($simpan_data,200); 

        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

}
