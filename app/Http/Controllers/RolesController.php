<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use App\Repositories\RolesRepository;

class RolesController extends Controller
{

    private $roles;
    public function __construct(RolesRepository $rolesRepository)
    {
        $this->roles = $rolesRepository;
    }

    public function index() {
        return view('roles');
    }

    public function getAll(Request $request) {
        try {
            $data  = $this->roles->getAll($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function getOne(Request $request) {
        try {
            $data  = $this->roles->getOne($request->all());
            return response()->json($data,$data->status); 
        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

    public function create(Request $request)
    {
        try {
            $element_checks = [
                'role_name'        => 'required|string|unique:roles,role_name,'.$request->input('id'),
            ];
            $element_attributes = [
                'role_name'        => 'Role Name',
            ];

            $validator = Validator::make($request->all(), $element_checks)->setAttributeNames($element_attributes);
            if ($validator->fails()) {
                $res["result"]  = false;
                $res["msg"]     = $validator->messages()->all();
                return response()->json($res);
            }

            $simpan_data = $this->roles->create($request->all());
            return response()->json($simpan_data,200); 

        } catch(Exception $e) {
            return response()->json($e,500); 
        }
    }

}
