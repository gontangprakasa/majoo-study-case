<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Roles;

class RolesRepository
{
    public function __construct()
    {

    }   

    public function getAll($request = array())
    {
        $ret                    = (object) [];
        $ret->draw              = $request["draw"];
        $ret->recordsTotal      = 0;
        $ret->recordsFiltered   = 0;
        $ret->data              = array();
        $ret->status            = 200;

        try {
            $keywoard = $request['keyword'];
            $query    = Roles::where(function($q) use ($keywoard){
                if( $keywoard<> "" ){
                    $q->where("role_name", "like", "%".$keywoard."%");
                }
            });

            $ret->recordsTotal      = $query->get()->count();
            $ret->recordsFiltered   = $query->get()->count();
            foreach($request["order"] as $i=>$order){
                $query = $query->orderBy($order["column"], $order["dir"]);
            }
            $ret->data = $query->skip($request["start"])->take($request["length"])->get()->toArray();
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->status = 400;
            return $ret;
        }
    }

    public function create($request = array())
    {
        $ret         = (object) [];
        $ret->status = 200;
		$ret->result = true;
		$ret->msg    = "";

        $pdo = DB::connection()->getPdo();
		$pdo->exec("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE");
		DB::beginTransaction();
        try {
            if ( $request['mode'] == "add" && !is_null($request['role_name']) ) {
                $check_suggestion = Roles::whereRaw("LOWER(role_name) like ?", ['%'.strtolower($request['category_name']).'%'])->first();
                if ($check_suggestion) {
                    $ret->msg    = "Role ".$request['role_name']." sudah ada";
                    $ret->status = 400;
                    $ret->result = false;
                    return $ret;
                }
                
                $save_data             = new Roles();
                $save_data->created_at = date("Y-m-d H:i:s");
                $ret->msg              = "Role ".$request['role_name']." berhasil dibuat";
            } else {
                $save_data             = Roles::where('id',$request['id'])->first();
                $save_data->updated_at = date("Y-m-d H:i:s");
                $ret->msg              = "Role ".$request['role_name']." berhasil diperbaharui";
            }
            
            $save_data->role_name  = $request['role_name'];
            $save_data->save();

            $ret->status = 200;
            $ret->result = true;
            DB::commit();
        } catch(QueryException $e){
			DB::rollback();
            $ret->status = 400;
			$ret->result = false;
			$ret->msg    = $e->getMessage();
		}
        return $ret;
    }

    public function getOne($request = array())
    {
        $ret = (object) [];
        $ret->result = true;
        $ret->msg = "Data ditemukan";
        $ret->data = null;
        $ret->status = 200;
        try {
            $query = Roles::where('id',$request['id'])->first();
            if (is_null($query)) {
                $ret->result = false;
                $ret->msg = "Data tidak ditemukan";
                $ret->data = null;
                $ret->status = 404;
            }

            $ret->data = $query;
           
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->msg    = $e->getMessage();
            $ret->status = 400;
            return $ret;
        }
    }

    
}