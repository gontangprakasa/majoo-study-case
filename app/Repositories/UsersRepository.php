<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Users;

class UsersRepository
{
    public function __construct()
    {

    }   

    public function login($request = array())
    {
        $ret 			= (object) [];
		$ret->result 	= true;
		$ret->msg 		= "";
        $ret->status    = 200;
        try {
            $email			= $request['txtEmail'];
            $password		= $request['txtPassword'];

            $credential = [
                'email' 		=> $email,
                'password' 		=> $password,
                'deleted_at' 	=> null,
                'deleted_by' 	=> 0,
            ];

            if (Auth::attempt($credential)) {
                $lastLogin = Users::where('email', $email)
                    ->update([
                    'updated_at' => date('Y-m-d H:i:s')
                    ]);
                
                $users = Users::where('email', $email)->first();
                if ($users['role_id'] == 1) {
                    $redirect = url('/dashboard');
                    if (session()->has('from')) {
                        $redirect = session()->pull('from');
                        if ($redirect == url('/login')) {
                            $redirect = url('/dashboard');
                        }
                    }
                } else {
                    $redirect = url('/');
                    if (session()->has('from')) {
                        $redirect = session()->pull('from');
                        if ($redirect == url('/login')) {
                            $redirect = url('/');
                        }
                    }
                }

             
                $ret->result = true;
                $ret->redirect = $redirect;
                $ret->msg = 'Login berhasil';
            } else {
                $ret->msg = 'Email atau password salah.';
                $ret->result = false;
                $ret->status = 200;
            }
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->status = 400;
        }
        return $ret;

    }

    public function register($request = array())
    {
        $ret         = (object) [];
        $ret->status = 200;
		$ret->result = true;
		$ret->msg    = "";
        $ret->redirect = "";

        $pdo = DB::connection()->getPdo();
		$pdo->exec("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE");
		DB::beginTransaction();
        try {
            $email =  $request['email'];
            $password = $request['password'];
            $save_data             = new Users();
            $save_data->created_at = date("Y-m-d H:i:s");
            $save_data->fullname  = $request['full_name'];
            $save_data->email     = $email;
            $save_data->password  = bcrypt($password);
            $save_data->role_id   = 2;
            $save_data->save();
            DB::commit();

            $credential = [
                'email' 		=> $email,
                'password' 		=> $password,
                'deleted_at' 	=> null,
                'deleted_by' 	=> 0,
            ];

            if (Auth::attempt($credential)) {
                $ret->msg    = "User ".$request['full_name']." berhasil dibuat";
                $ret->status = 200;
                $ret->result = true;
                $ret->redirect = url('/');
            } else {
                $ret->msg = 'Gagal menambahkan, silahkan coba kembali.';
                $ret->result = false;
                $ret->status = 200;
            }
        } catch(QueryException $e){
			DB::rollback();
            $ret->status = 400;
			$ret->result = false;
			$ret->msg    = $e->getMessage();
		}
        return $ret;
    }

    public function getAll($request = array())
    {
        $ret                    = (object) [];
        $ret->draw              = $request["draw"];
        $ret->recordsTotal      = 0;
        $ret->recordsFiltered   = 0;
        $ret->data              = array();
        $ret->status            = 200;

        try {
            $keywoard = $request['keyword'];
            $query    = Users::select([
                'users.*',
                'roles.role_name'
            ])
            ->join('roles','roles.id','users.role_id')
            ->where('users.id','!=',Auth::user()->id)
            ->where(function($q) use ($keywoard){
                if( $keywoard<> "" ){
                    $q->where("fullname", "like", "%".$keywoard."%");
                }
            });

            $ret->recordsTotal      = $query->get()->count();
            $ret->recordsFiltered   = $query->get()->count();
            foreach($request["order"] as $i=>$order){
                $query = $query->orderBy($order["column"], $order["dir"]);
            }
            $ret->data = $query->skip($request["start"])->take($request["length"])->get()->toArray();
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->status = 400;
            return $ret;
        }
    }

    public function getBy($role_name,$request = array())
    {
        $ret                    = (object) [];
        $ret->draw              = $request["draw"];
        $ret->recordsTotal      = 0;
        $ret->recordsFiltered   = 0;
        $ret->data              = array();
        $ret->status            = 200;

        try {
            $keywoard = $request['keyword'];
            $query    = Users::select([
                'users.*',
                'roles.role_name'
            ])
            ->join('roles','roles.id','users.role_id')
            ->where('role_name',$role_name)
            ->where('users.id','!=',Auth::user()->id)
            ->where(function($q) use ($keywoard){
                if( $keywoard<> "" ){
                    $q->where("fullname", "like", "%".$keywoard."%");
                }
            });

            $ret->recordsTotal      = $query->get()->count();
            $ret->recordsFiltered   = $query->get()->count();
            foreach($request["order"] as $i=>$order){
                $query = $query->orderBy($order["column"], $order["dir"]);
            }
            $ret->data = $query->skip($request["start"])->take($request["length"])->get()->toArray();
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->status = 400;
            return $ret;
        }
    }

    public function create($request = array())
    {
        $ret         = (object) [];
        $ret->status = 200;
		$ret->result = true;
		$ret->msg    = "";

        $pdo = DB::connection()->getPdo();
		$pdo->exec("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE");
		DB::beginTransaction();
        try {
            if ( $request['mode'] == "add" && !is_null($request['fullname']) ) {    
                $save_data             = new Users();
                $save_data->created_at = date("Y-m-d H:i:s");
                $ret->msg              = "User ".$request['fullname']." berhasil dibuat";
            } else {
                $save_data             = Users::where('id',$request['id'])->first();
                $save_data->updated_at = date("Y-m-d H:i:s");
                $ret->msg              = "User ".$request['fullname']." berhasil diperbaharui";
            }
            
            $save_data->fullname  = $request['fullname'];
            $save_data->role_id   = $request['role_id'];
            $save_data->save();

            $ret->status = 200;
            $ret->result = true;
            DB::commit();
        } catch(QueryException $e){
			DB::rollback();
            $ret->status = 400;
			$ret->result = false;
			$ret->msg    = $e->getMessage();
		}
        return $ret;
    }

    public function getOne($request = array())
    {
        $ret = (object) [];
        $ret->result = true;
        $ret->msg = "Data ditemukan";
        $ret->data = null;
        $ret->status = 200;
        try {
            $query = Users::select([
                'users.*',
                'roles.role_name'
            ])
            ->join('roles','roles.id','users.role_id')
            ->where('users.id',$request['id'])
            ->first();

            if (is_null($query)) {
                $ret->result = false;
                $ret->msg = "Data tidak ditemukan";
                $ret->data = null;
                $ret->status = 404;
            }

            $ret->data = $query;
           
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->msg    = $e->getMessage();
            $ret->status = 400;
            return $ret;
        }
    }

    public function changePassword($request = array())
    {
        $ret         = (object) [];
        $ret->status = 200;
		$ret->result = true;
		$ret->msg    = "";

        $pdo = DB::connection()->getPdo();
		$pdo->exec("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE");
		DB::beginTransaction();
        try {
            $old_password   = $request['old_password'];
            $password       = $request['password'];
            $email          = $request['email'];
            $credential = [
                'email' 		=> $email,
                'password' 		=> $old_password,
                'deleted_at' 	=> null,
                'deleted_by' 	=> 0,
            ];
            
            if (Auth::attempt($credential)) {
                $save_data             = Users::where('id',Auth::user()->id)->first();
                if ($save_data) {
                    $save_data->updated_at  = date("Y-m-d H:i:s");
                    $save_data->password    = bcrypt($password);
                    $save_data->save();

                    $ret->msg              = "User ".$save_data['fullname']." berhasil diperbaharui";
                    $ret->status = 200;
                    $ret->result = true;
                    DB::commit();
                } else {
                    $ret->result = false;
                    $ret->msg = "Data tidak ditemukan";
                    $ret->data = null;
                    $ret->status = 404;
                }
            } else {
                $ret->result = false;
                    $ret->msg = "Maaf password anda salah!!!";
                    $ret->data = null;
                    $ret->status = 400;
            }
         
        } catch(QueryException $e){
			DB::rollback();
            $ret->status = 400;
			$ret->result = false;
			$ret->msg    = $e->getMessage();
		}
        return $ret;
    }

    
}