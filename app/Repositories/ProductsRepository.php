<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\Models\Products;
use App\Models\ProductUser;

class ProductsRepository
{
    public function __construct()
    {

    }   

    public function getAll($request = array())
    {
        $ret                    = (object) [];
        $ret->draw              = $request["draw"];
        $ret->recordsTotal      = 0;
        $ret->recordsFiltered   = 0;
        $ret->data              = array();
        $ret->status            = 200;

        try {
            $keywoard = $request['keyword'];
            $query    = Products::select([
                'products.*',
                'categories.category_name'
            ])
            ->join("categories","categories.id","products.category_id")
            ->WhereNull('deleted_at')
            ->where(function($q) use ($keywoard){
                if( $keywoard<> "" ){
                    $q->where("category_name", "like", "%".$keywoard."%");
                }
            });

            $ret->recordsTotal      = $query->get()->count();
            $ret->recordsFiltered   = $query->get()->count();
            foreach($request["order"] as $i=>$order){
                $query = $query->orderBy($order["column"], $order["dir"]);
            }
            $ret->data = $query->skip($request["start"])->take($request["length"])->get()->toArray();
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->status = 400;
            return $ret;
        }
    }

    public function create($request = array(), $hasFile, $file)
    {
        $ret         = (object) [];
        $ret->status = 200;
		$ret->result = true;
		$ret->msg    = "";

        $pdo = DB::connection()->getPdo();
		$pdo->exec("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE");
		DB::beginTransaction();
        try {

            $dir_upload = '/uploads/product/';
            $dir        = base_path().'/public'.$dir_upload;
            if (! File::exists($dir)) {
                File::makeDirectory($dir,0777,true);
            }

            if ( $request['mode'] == "add" && !is_null($request['product_name']) ) {
                $check_suggestion = Products::whereRaw("LOWER(product_name) like ?", ['%'.strtolower($request['product_name']).'%'])->first();
                if ($check_suggestion) {
                    $ret->msg    = "Product ".$request['product_name']." sudah ada";
                    $ret->status = 400;
                    $ret->result = false;
                    return $ret;
                }
                
                $save_data             = new Products();
                $save_data->created_at = date("Y-m-d H:i:s");
                $ret->msg              = "Product ".$request['product_name']." berhasil dibuat";
            } else {
                $save_data             = Products::where('id',$request['id'])->first();
                $save_data->updated_at = date("Y-m-d H:i:s");
                $ret->msg              = "Product ".$request['product_name']." berhasil diperbaharui";
            }

            if($hasFile){
                if(file_exists($dir.$save_data->file)){
                    Storage::delete($dir.$save_data->file);
                };
                $resorce_file    = $file;
                $nama_file       = 'product-'.time().'.'.$resorce_file->getClientOriginalExtension();
                $save_data->file = $dir_upload.$nama_file;
                $resorce_file->move($dir, $nama_file);
            }

            $save_data->description  = $request['description'];
            $save_data->price        = preg_replace('/\D/','',$request['price']);
            $save_data->category_id  = $request['category_id'];
            $save_data->product_name = $request['product_name'];
            $save_data->save();

            $ret->status = 200;
            $ret->result = true;
            DB::commit();
        } catch(QueryException $e){
			DB::rollback();
            $ret->status = 400;
			$ret->result = false;
			$ret->msg    = $e->getMessage();
		}
        return $ret;
    }

    public function getOne($request = array())
    {
        $ret = (object) [];
        $ret->result = true;
        $ret->msg = "Data ditemukan";
        $ret->data = null;
        $ret->status = 200;
        try {
            $query = Products::select([
                'products.*',
                'categories.category_name'
            ])
            ->join("categories","categories.id","products.category_id")
            ->where('products.id',$request['id'])->first();
            if (is_null($query)) {
                $ret->result = false;
                $ret->msg = "Data tidak ditemukan";
                $ret->data = null;
                $ret->status = 404;
            }

            $ret->data = $query;
           
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->msg    = $e->getMessage();
            $ret->status = 400;
            return $ret;
        }
    }

    public function loadProduct($request = array())
    {
        $ret = (object) [];
        $ret->result = true;
        $ret->msg = "Data ditemukan";
        $ret->data = null;
        $ret->status = 200;
        try {
            $query = Products::select([
                'products.*',
                'categories.category_name'
            ])
            ->join("categories","categories.id","products.category_id")
            ->whereNull('deleted_at')
            ->get();

            $ret->data = $query;
           
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->msg    = $e->getMessage();
            $ret->status = 400;
            return $ret;
        }
    }

    public function loadListProductBy($request = array())
    {

        $ret                    = (object) [];
        $ret->draw              = @$request["draw"];
        $ret->recordsTotal      = 0;
        $ret->recordsFiltered   = 0;
        $ret->data              = array();
        $ret->status            = 200;

        try {
            $keywoard = @$request['keyword'];
            $query = ProductUser::select([
                'products.*',
                'categories.category_name',
                'product_users.quality',
                DB::Raw('(product_users.quality * products.price) as count_price')
            ])
            ->join("products", "product_users.product_id", "products.id")
            ->join("categories","categories.id","products.category_id")
            ->where("product_users.user_id", $request['id'])
            ->where(function($q) use ($keywoard){
                if( $keywoard<> "" ){
                    $q->where("products.product_name", "like", "%".$keywoard."%");
                    $q->orWhere("categories.category_name", "like", "%".$keywoard."%");
                }
            });
            if (@$request["order"]) {
                $ret->recordsTotal      = $query->get()->count();
                $ret->recordsFiltered   = $query->get()->count();
                foreach($request["order"] as $i=>$order){
                    $query = $query->orderBy($order["column"], $order["dir"]);
                }
                $ret->data = $query->skip($request["start"])->take($request["length"])->get()->toArray();
            } else {
                $ret->data = $query->get()->toArray();
            }
           
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->status = 400;
            return $ret;
        }

        $ret = (object) [];
        $ret->result = true;
        $ret->msg = "Data ditemukan";
        $ret->data = null;
        $ret->status = 200;
        try {
            
           
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->msg    = $e->getMessage();
            $ret->status = 400;
            return $ret;
        }
    }

    public function createOrder($request)
    {
        $ret         = (object) [];
        $ret->status = 200;
		$ret->result = true;
		$ret->msg    = "";

        $pdo = DB::connection()->getPdo();
		$pdo->exec("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE");
		DB::beginTransaction();
        try {
            $user_id    =  Auth::user()->id;
            $product_id = $request['product_id'];
            $save_data  = ProductUser::where('product_id', $product_id)->where('user_id', $user_id)->first();
            if ($save_data) {
                $save_data->quality     = $save_data['quality'] + $request['quality'];
                $save_data->updated_at  = date("Y-m-d H:i:s");
            } else {
                $save_data              = new ProductUser();
                $save_data->quality     = $request['quality'];
                $save_data->product_id  = $product_id;
                $save_data->created_at  = date("Y-m-d H:i:s");
            }
            $save_data->user_id = $user_id;
            
            $save_data->save();

            $ret->status = 200;
            $ret->result = true;
            DB::commit();
        } catch(QueryException $e){
			DB::rollback();
            $ret->status = 400;
			$ret->result = false;
			$ret->msg    = $e->getMessage();
		}
        return $ret;
    }

    public function getOneByUser($request = array())
    {
        $ret                    = (object) [];
        $ret->draw              = $request["draw"];
        $ret->recordsTotal      = 0;
        $ret->recordsFiltered   = 0;
        $ret->data              = array();
        $ret->status            = 200;

        try {
            $keywoard = $request['keyword'];
            $query    = Products::select([
                'products.*',
                'categories.category_name'
            ])
            ->join("categories","categories.id","products.category_id")
            ->where(function($q) use ($keywoard){
                if( $keywoard<> "" ){
                    $q->where("category_name", "like", "%".$keywoard."%");
                }
            });

            $ret->recordsTotal      = $query->get()->count();
            $ret->recordsFiltered   = $query->get()->count();
            foreach($request["order"] as $i=>$order){
                $query = $query->orderBy($order["column"], $order["dir"]);
            }
            $ret->data = $query->skip($request["start"])->take($request["length"])->get()->toArray();
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->status = 400;
            return $ret;
        }
    }

    public function delete($request)
    {
        $ret = (object) [];
		$ret->result = true;
		$ret->msg = "";
        $ret->status = 200;

        $pdo = DB::connection()->getPdo();
		$pdo->exec("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE");
		DB::beginTransaction();
        try{
            $id = $request['id'];
            $data = Products::where('id', $id)->first(); 
            $data->deleted_by = Auth::user()->id;
            $data->deleted_at = date("Y-m-d H:i:s");
            $data->save();

            DB::commit();
            $ret->result = true;
            $ret->msg = "Product berhasil dihapus";

		}catch(QueryException $e){
            DB::rollback();
			$ret->result = false;
			$ret->msg = $e->getMessage();
            $ret->status = 500;
		}
        return $ret;
    }
    
    
}