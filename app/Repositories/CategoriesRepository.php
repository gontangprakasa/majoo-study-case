<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Categories;

class CategoriesRepository
{
    public function __construct()
    {

    }   

    public function getAll($request = array())
    {
        $ret                    = (object) [];
        $ret->draw              = @$request["draw"];
        $ret->recordsTotal      = 0;
        $ret->recordsFiltered   = 0;
        $ret->data              = array();
        $ret->status            = 200;

        try {
            $keywoard = $request['keyword'];
            $query    = Categories::where(function($q) use ($keywoard){
                if( $keywoard<> "" ){
                    $q->where("category_name", "like", "%".$keywoard."%");
                }
            });

            $ret->recordsTotal      = $query->get()->count();
            $ret->recordsFiltered   = $query->get()->count();
            foreach($request["order"] as $i=>$order){
                $query = $query->orderBy($order["column"], $order["dir"]);
            }
            $ret->data = $query->skip($request["start"])->take($request["length"])->get()->toArray();
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->status = 400;
            return $ret;
        }
    }

    public function getCount()
    {
        $ret                    = (object) [];
        $ret->data              = array();
        $ret->status            = 200;
        try {
            $ret->data    = Categories::count();
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->status = 400;
            return $ret;
        }

    }

    public function create($request = array())
    {
        $ret         = (object) [];
        $ret->status = 200;
		$ret->result = true;
		$ret->msg    = "";

        $pdo = DB::connection()->getPdo();
		$pdo->exec("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE");
		DB::beginTransaction();
        try {
            if ( $request['mode'] == "add" && !is_null($request['category_name']) ) {
                $check_suggestion = Categories::whereRaw("LOWER(category_name) like ?", ['%'.strtolower($request['category_name']).'%'])->first();
                if ($check_suggestion) {
                    $ret->msg    = "Category ".$request['category_name']." sudah ada";
                    $ret->status = 400;
                    $ret->result = false;
                    return $ret;
                }
                
                $save_data             = new Categories();
                $save_data->created_at = date("Y-m-d H:i:s");
                $ret->msg              = "Category ".$request['category_name']." berhasil dibuat";
            } else {
                $save_data             = Categories::where('id',$request['id'])->first();
                $save_data->updated_at = date("Y-m-d H:i:s");
                $ret->msg              = "Category ".$request['category_name']." berhasil diperbaharui";
            }
            
            $save_data->category_name  = $request['category_name'];
            $save_data->save();

            $ret->status = 200;
            $ret->result = true;
            DB::commit();
        } catch(QueryException $e){
			DB::rollback();
            $ret->status = 400;
			$ret->result = false;
			$ret->msg    = $e->getMessage();
		}
        return $ret;
    }

    public function getOne($request = array())
    {
        $ret = (object) [];
        $ret->result = true;
        $ret->msg = "Data ditemukan";
        $ret->data = null;
        $ret->status = 200;
        try {
            $query = Categories::where('id',$request['id'])->first();
            if (is_null($query)) {
                $ret->result = false;
                $ret->msg = "Data tidak ditemukan";
                $ret->data = null;
                $ret->status = 404;
            }

            $ret->data = $query;
           
            return $ret;
        } catch(Exception $e) {
            $ret->data   = array();
            $ret->msg    = $e->getMessage();
            $ret->status = 400;
            return $ret;
        }
    }

    public function delete($request)
    {
        $ret = (object) [];
		$ret->result = true;
		$ret->msg = "";
        $ret->status = 200;

        $pdo = DB::connection()->getPdo();
		$pdo->exec("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE");
		DB::beginTransaction();
        try{
            $id = $request['id'];
			$check_suggestion = Categories::where('categories.id',$id)
            ->join('products','categories.id','products.category_id')->get();
            
            if (count($check_suggestion) > 0) {
                    $ret->result = false;
                    $ret->msg = "Gagal menghapus masih ada product yang aktif";
            } else {
                Categories::where('id', $id)->delete(); 
                $ret->result = true;
                $ret->msg = "Category berhasil dihapus";
                DB::commit();
            }
		}catch(QueryException $e){
            DB::rollback();
			$ret->result = false;
			$ret->msg = $e->getMessage();
            $ret->status = 500;
		}
        return $ret;
    }

    
}