<?php

use Illuminate\Database\Seeder;
use App\Models\Roles;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            Roles Type
        */

        $RoleItems = [
            [
                'role_name'     => 'Admin',
            ],
            [
                'role_name'     => 'Customer',
            ],
        ];

        /*
           Add Role Items
        */
        foreach ($RoleItems as $RoleItem) {
            Roles::insert([
                'role_name'     => $RoleItem['role_name']
            ]);
        }
    }
}
