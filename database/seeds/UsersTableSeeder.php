<?php

use Illuminate\Database\Seeder;
use App\Models\Users;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Users::insert([
            'fullname'     => 'Super Admin',
            'role_id'       => 1,
            'email'         => 'admin@mail.com',
            'password'      => bcrypt('password'),
            'remember_token'=> NULL,
            'created_at'    => date('Y-m-d H:i:s'),
            'created_by'    => 1
        ]);
    }
}
