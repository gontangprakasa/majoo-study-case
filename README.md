# Majoo Test Case
System Toko Online.

# Daftar Isi
* [Prakata](#prakata)
* [Fitur](#fitur)
    * [Configuration](#configuration)
    * [Database](#database)
* [Cara Menjalankan](#cara-menjalankan)
    * [Prerequisites](#prerequisites)
    * [Persiapan](#persiapan)
    * [Menjalankan Project](#menjalankan-project)
* [ERD](#erd)

# Prakata
Dalam pengembangan aplikasi, diperlukan struktur dasar yang dapat disepakati dan dipelajari
oleh seluruh anggota tim development. Sehingga dibuatlah menggunakan framework [Laravel](https://laravel.com/) ini untuk memenuhi
kebutuhan tersebut.

# Fitur
Berikut ini adalah fitur-fitur yang terdapat dalam sistem toko online ini.

### Configuration
Agar aplikasi dapat berjalan dengan parameter-parameter tertentu, sebaiknya dapat memanfaatkan
konfigurasi manajemen. Berikut ini modul yang digunakan terkait konfigurasi.
| Modul | Kegunaan |
|-|-|
| [Laravel Config](https://laravel.com/docs/6.x/configuration) | memanajemen konfigurasi |


### Database
Sistem ini memanfaatkan database management system (DBMS).
Pemanfaatan database ini dibantu ORM (Object-Relational Mapping) dengan rangkaian modul
berikut ini.
| Modul | Kegunaan |
|-|-|
| [Laravel TypeORM](https://laravel.com/docs/6.x/queries) | sebagai ORM |
| [MYSQL](https://lumen.laravel.com/docs/6.x/database) | menyambungkan ke DBMS MYSQL |

# Cara Menjalankan
Berikut ini hal-hal yang perlu dilakukan agar project ini dapat dijalankan.

## Prerequisites
Sistem ini membutuhkan sistem lain seperti DBMS, cache database, dll. Agar proses
persiapan sampai menjalankan project dapat berjalan lancar, berikut ini yang harus
diinstall lebih dahulu.
| Sistem Lain | Kegunaan |
|-|-|
| [Git](http://git-scm.com) | manajemen versi |
| [PHP v7.4](https://www.php.net/manual/en/install.php) | sebagai mesin yang menjalankan project |
| [Composer](https://getcomposer.org/) | memanajemen modul dalam project |
| [MYSQL Ver 14.14 Distrib 5.7.35](https://www.mysql.com/) | memanajemen data secara permanen |

## Persiapan
Sebelum project dapat dijalankan, ada beberapa hal yang perlu dipersiapkan.
Berikut ini langkah-langkah untuk mempersiapkan project.

1. Clone repository ini dengan menjalankan perintah berikut di terminal emulator.
```sh
git init
git clone https://gitlab.com/gontangprakasa/majoo-study-case.git
```
2. Masuk ke direktory project dengan menjalankan perintah berikut ini.
```sh
cd ./majoo-study-case
```
3. Install modul-modul yang dibutuhkan project dengan menjalankan perintah berikut ini.
```sh
# dengan composer
composer install and composer update
```

5. Buat file bernama `.env` dan isi dengan konfigurasi yang diinginkan.
> Contoh isi file `.env` ini dapat di lihat di file [.env.example](./.env.example)

4. Buat databse sesuai dengan .env yang telah diatur

6. Generate table yang dibutuhkan dengan menjalankan perintah berikut ini.
```sh
php artisan migrate
```

7. Running autoSeeder data yang dibutuhkan dengan menjalankan perintah berikut ini.
```sh
php artisan db:seed
```

## Menjalankan Project
Setelah project siap, selanjutnya dapat dijalankan dengan menggunakan perintah berikut ini.
```sh
php artisan serve
```


# Entity Relationship Diagram, Activity Diagram, Use Case Diagram
Dalam sistem ini, sudah semestinya sebelum melakukan development perlu adanya proses bisnis ataupun dokumentasi yang dirancang.
Untuk melihat ERD yang sudah dibuat, silakan buka file [documentation/](./documentation).

