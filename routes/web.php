<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// home
Route::get('/', ['as' => 'home', 'uses' => 'PageController@index']);
Route::group(['prefix' => 'page'], function(){
	Route::post('/loadProducts', ['as' => 'load_product', 'uses' => 'PageController@loadProduct']);
});
// Backoffice
Route::group(['prefix' => ''], function() {
	Route::group(['middleware' => 'guest'], function() {
		Route::get('/login', ['as' => 'login', 'uses' => 'LoginController@index']);
		Route::post('/login', ['as' => 'login', 'uses' => 'LoginController@login']);

		Route::get('/register', ['as' => 'register', 'uses' => 'RegisterController@index']);
		Route::post('/register', ['as' => 'register', 'uses' => 'RegisterController@register']);
	});

	Route::group(['middleware' => 'auth'], function() {
		
		/* Logout */
		Route::get('/logout', 'LoginController@logout');

		Route::group(['prefix' => 'change-password'], function(){
			Route::get('', 'ChangePasswordController@index');
			Route::post('', 'ChangePasswordController@create');
		});
		// Start
		Route::group(['middleware' => ['check.role:1']], function(){
			Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
			Route::group(['prefix' => 'categories'], function(){
				Route::get('', ['as' => 'categories', 'uses' => 'CategoriesController@index']);
				Route::post('/getAll', ['as' => 'all_category', 'uses' => 'CategoriesController@getAll']);
				Route::post('/getOne', ['as' => 'one_category', 'uses' => 'CategoriesController@getOne']);
				Route::post('/create', ['as' => 'create_category', 'uses' => 'CategoriesController@create']);
				Route::post('/delete', ['as' => 'delete_category', 'uses' => 'CategoriesController@delete']);
			});

			Route::group(['prefix' => 'products'], function(){
				Route::get('', ['as' => 'product', 'uses' => 'ProductsController@index']);
				Route::post('/getAll', ['as' => 'all_product', 'uses' => 'ProductsController@getAll']);
				Route::post('/getOne', ['as' => 'one_product', 'uses' => 'ProductsController@getOne']);
				Route::post('/create', ['as' => 'create_product', 'uses' => 'ProductsController@create']);
				Route::post('/delete', ['as' => 'delete_product', 'uses' => 'ProductsController@delete']);
			});

			Route::group(['prefix' => 'products-user'], function(){
				Route::get('', ['as' => 'product', 'uses' => 'ProductsUserController@index']);
				Route::get('/{id}', ['as' => 'product_detail', 'uses' => 'ProductsUserController@getIndexOne']);
				Route::post('/getAll', ['as' => 'all_product', 'uses' => 'ProductsUserController@getAll']);
				Route::post('/getOne', ['as' => 'one_product', 'uses' => 'ProductsUserController@getOne']);
				Route::post('/create', ['as' => 'create_product', 'uses' => 'ProductsUserController@createOrder']);
			});

			Route::group(['prefix' => 'users'], function(){
				Route::get('', ['as' => 'product', 'uses' => 'UsersController@index']);
				Route::post('/getAll', ['as' => 'all_users', 'uses' => 'UsersController@getAll']);
				Route::post('/getOne', ['as' => 'one_users', 'uses' => 'UsersController@getOne']);
				Route::post('/create', ['as' => 'create_users', 'uses' => 'UsersController@create']);
			});

			Route::group(['prefix' => 'roles'], function(){
				Route::get('', ['as' => 'product', 'uses' => 'RolesController@index']);
				Route::post('/getAll', ['as' => 'all_users', 'uses' => 'RolesController@getAll']);
				Route::post('/getOne', ['as' => 'one_users', 'uses' => 'RolesController@getOne']);
				Route::post('/create', ['as' => 'create_users', 'uses' => 'RolesController@create']);
			});
		});
		
		Route::group(['middleware' => ['check.role:1|2']], function(){
			Route::group(['prefix' => 'page'], function(){
				Route::get('/listProductBy', ['as' => 'load_product', 'uses' => 'PageController@loadProductBy']);
				Route::post('/loadListProductBy', ['as' => 'load_product', 'uses' => 'PageController@loadListProductBy']);
			});
		});
	});
});



